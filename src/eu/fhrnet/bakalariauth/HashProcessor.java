package eu.fhrnet.bakalariauth;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.apache.commons.codec.digest.MessageDigestAlgorithms;



public class HashProcessor {
    private final String prehash;
    private final String h512;

    public HashProcessor(StepOneAuthHolder ah, String password) {
        this.prehash = "" + ah.getSalt() + ah.getInternCode() + ah.getRole() + password;
		this.h512 = computeH512(this.prehash);
    }

    public String getH512() {
        return this.h512;
    }

    public static String computeH512(String hash, boolean adapted) {
        String h512 = computeH512(hash);
        if (!adapted) {
            return h512;
        }
        h512 = h512.replace('\\', '_').replace('+', '-').replace("/", "_");
        if(Config.debug)
        		System.out.println("Adapted Base64: " + h512);
        return h512;
    }

    private static String computeH512(String hash) {
        byte[] byteValue = hash.getBytes(Charset.forName("UTF-8"));
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance(MessageDigestAlgorithms.SHA_512);
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Hashing function for SHA-512 not available.");
        }
        if (digest == null) {
            return null;
        }
        byte[] byteHash = digest.digest(byteValue);
        if(Config.debug)
        		System.out.println("hashhhhh: " + new String(byteHash));
        String base64 = Base64.getEncoder().encodeToString(byteHash);
        if(Config.debug)
        		System.out.println("Base64-sed SHA-512 hash: " + base64);
        return base64;
    }
}