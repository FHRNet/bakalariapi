package eu.fhrnet.bakalariauth;

public class StepOneAuthHolder {
    private Iface iface;
    private String internCode;
    private String role;
    private String salt;

    public StepOneAuthHolder(String salt, String internCode, String role, Iface iface) {
        this.salt = salt;
        this.internCode = internCode;
        this.role = role.substring(0, 1);
        this.iface = iface;
    }

    public String getSalt() {
        return this.salt;
    }

    public String getInternCode() {
        return this.internCode;
    }

    public String getRole() {
        return this.role;
    }

    public Iface getIface() {
        return this.iface;
    }

    public String toString() {
        return String.format("StepOneAuthHolder[salt=%s,internCode=%s,role=%s,iface=%s]", new Object[]{this.salt, this.internCode, this.role, this.iface.name()});
    }
}
