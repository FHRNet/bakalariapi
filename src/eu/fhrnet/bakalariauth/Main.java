package eu.fhrnet.bakalariauth;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

@WebServlet(name = "BakalariApi", value = "/getToken")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/plain");
		
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		
		if (user == null || pass == null) {
			response.getWriter().println("Not enough parameters.\nI need GET user & pass");
		} else {
			String token;
			try {
				token = AuthenticationUtils.authenticate(Config.url, user, pass, "", "").getToken();
				response.getWriter().println(AuthenticationUtils.getAuthToken(token));
			} catch (IllegalAccessException | SAXException | ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
	}
}

/*public class Main {
	public static void main(String[] args) throws IllegalAccessException, IOException, SAXException, ParserConfigurationException {
		System.out.println("Bakalari auth starting...");
		
		String token = AuthenticationUtils.authenticate(Config.url, Config.username, Config.password, "", "").getToken();
		System.out.println("Token: " + AuthenticationUtils.getAuthToken(token));
	}
}*/
