package eu.fhrnet.bakalariauth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class AuthenticationUtils {
    public static class LoginInformation {
        private Iface authIfaceVersion;
        private String authTokenBase;
        private String httpPassword;
        private String httpUsername;

        public LoginInformation(AuthenticationBaseInfo authTokenBase) {
            this.authTokenBase = authTokenBase.getToken();
            this.authIfaceVersion = authTokenBase.getIface();
        }

        public String getAuthTokenBase() {
            return this.authTokenBase;
        }

        public Iface getAuthIfaceVersion() {
            return this.authIfaceVersion;
        }

        public void setHttpCredentials(String httpUsername, String httpPassword) {
            this.httpUsername = httpUsername;
            this.httpPassword = httpPassword;
        }

        public String getHttpPassword() {
            return this.httpPassword;
        }

        public String getHttpUsername() {
            return this.httpUsername;
        }
    }
    
    private static String loadHashPrerequisites(String urlString, String username, String httpUsername, String httpPassword, Iface ifaceVersion) throws IOException {
        URL uri = new URL(urlString + "login.aspx?gethx=" + URLEncoder.encode(username, "UTF-8"));
        if(Config.debug)
        		System.out.println("Auth step one URL: " + uri.toExternalForm());
        HttpURLConnection client = (HttpURLConnection) uri.openConnection();
        client.setRequestMethod("GET");
        client.setDoOutput(true);
        
        BufferedReader in = new BufferedReader(
        		new InputStreamReader(client.getInputStream()));
        String line, content = "";
        
        while ((line = in.readLine()) != null) {
        		content += line;
        }
        		
        return content;
    }

    private static HashProcessor processStepTwo(StepOneAuthHolder authHolder, String password) {
        return new HashProcessor(authHolder, password);
    }
    
	private static StepOneAuthHolder processStepOne(String urlString, String username, String httpUsername, String httpPassword, Iface ifaceVersion) throws IOException, IllegalAccessException, SAXException, ParserConfigurationException {
		String content = loadHashPrerequisites(urlString, username, httpUsername, httpPassword, ifaceVersion);
        if (ifaceVersion == Iface.VERSION_01) {
            return AuthenticationIfaceV01.completeStepOneAuthHolder(username, content);
        }
        return null;
    }
	   
    private static String getAuthTokenBase(HashProcessor hashProcessor, String username) {
        return "*login*" + username + "*pwd*" + hashProcessor.getH512() + "*sgn*" + "ANDR";
	}
    
    public static String getAuthToken(String authTokenBase) {
    		SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
    		sd.setTimeZone(TimeZone.getTimeZone("Europe/Prague"));
        return HashProcessor.computeH512(authTokenBase + sd.format(new Date()), true);
    }
	
	public static AuthenticationBaseInfo authenticate(String url, String username, String password, String httpUsername, String httpPassword) throws IllegalAccessException, IOException, SAXException, ParserConfigurationException {
	    AuthenticationBaseInfo result;
	    result = new AuthenticationBaseInfo();
	    StepOneAuthHolder authHolder = processStepOne(url, username, httpUsername, httpPassword, Iface.VERSION_2);
	    if (authHolder == null) {
	        result.setIface(Iface.VERSION_01);
	        authHolder = processStepOne(url, username, httpUsername, httpPassword, Iface.VERSION_01);
	    }
	    result.setToken(getAuthTokenBase(processStepTwo(authHolder, password), username));
	    return result;
	}
}
