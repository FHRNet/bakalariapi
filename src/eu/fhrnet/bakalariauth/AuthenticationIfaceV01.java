package eu.fhrnet.bakalariauth;

import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class AuthenticationIfaceV01 {
    public static StepOneAuthHolder completeStepOneAuthHolder(String username, String content) throws IOException, IllegalAccessException, SAXException, ParserConfigurationException {
        try {
            Document doc = getDomDocument(content);
            if (doc.getElementsByTagName("res") == null || doc.getElementsByTagName("res").item(0) == null) {
                throw new IllegalAccessException("Illegal server version of Bakalari.");
            }
            String resultNumber = doc.getElementsByTagName("res").item(0).getTextContent();
            if ("2".equals(resultNumber) || "02".equals(resultNumber)) {
                System.out.println("Auth error");
            }
            String salt = doc.getElementsByTagName("salt").item(0).getTextContent();
            String iCode = doc.getElementsByTagName("ikod").item(0).getTextContent();
            String role = doc.getElementsByTagName("typ").item(0).getTextContent();
            if(Config.debug)
            		System.out.println("Auth step one successful.");
            return new StepOneAuthHolder(salt, iCode, role, Iface.VERSION_01);
        } catch (SAXParseException sax) {
            String saxMessage = sax.getMessage();
            if (saxMessage.contains("END_TAG")) {
            		System.out.println("Auth error");
            } else if (saxMessage.contains("DOCDECL") && content.contains("<html")) {
                throw new UnknownHostException("Školní server na zadané adrese nefunguje. Kontaktujte školu.");
            } else {
                throw sax;
            }
        } catch (SAXException sax2) {
            if (content.contains("action=\"login.aspx?gethx=" + username + "\"")) {
                throw new IllegalAccessException("Illegal server version of Bakalari.");
            }
            throw sax2;
        }
		return null;
    }

    private static Document getDomDocument(String content) throws IOException, ParserConfigurationException, SAXException, DOMException {
    		if(Config.debug)
    			System.out.println("Response: " + content);
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(content)));
        doc.getDocumentElement().normalize();
        return doc;
    }
}