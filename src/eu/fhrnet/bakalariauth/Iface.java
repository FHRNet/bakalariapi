package eu.fhrnet.bakalariauth;

public enum Iface {
    VERSION_01("1"),
    VERSION_2("2");
    
    private final String value;

    private Iface(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static Iface fromValue(String value) {
        return VERSION_01;
    }
}