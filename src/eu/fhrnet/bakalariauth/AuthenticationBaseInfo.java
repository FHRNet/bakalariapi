package eu.fhrnet.bakalariauth;

class AuthenticationBaseInfo {
    private Iface iface = Iface.VERSION_01;
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public void setIface(Iface iface) {
        this.iface = iface;
    }

    public String getToken() {
        return this.token;
    }

    public Iface getIface() {
        return this.iface;
    }
}